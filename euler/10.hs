solve :: Integral a => a -> a
solve n = sum $ primes n

sieve :: (Integral a) => [a] -> [a] -> [a]
sieve ps [] = ps
sieve ps (p:vs) = sieve (p:ps) [k | k <- vs, k `mod` p /= 0]

primes :: (Integral a) => a -> [a]
primes n = sieve [] [2..n]

isPrime :: (Integral a) => a -> Bool
isPrime n
    | n > 1 = null [k | k <- primes sqrt_n, n `mod` k == 0]
    | otherwise = False
	where sqrt_n = truncate $ sqrt $ fromIntegral n
