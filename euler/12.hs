import System.IO
import Data.List
import Control.Applicative

factors :: Integral a => a -> [a]
factors 0 = [1..]
factors n = [k | k <- [1..n], n `rem` k == 0]

numFactors :: Integral a => a -> Int
numFactors = (length . factors)

numTriangularFactors :: Integral a => [Int]
numTriangularFactors = [if even k then (numFactors $ k `div` 2) * (numFactors $ k + 1) else (numFactors $ (k + 1) `div` 2) * (numFactors k) | k <- [1..]]

main = getLine >>= print . (liftA (\n -> (n + 1)*(n + 2) `div` 2)) . (\n -> findIndex (> n) numTriangularFactors) . (read :: String -> Int)
