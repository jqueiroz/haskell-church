import System.IO

getIntgs :: Int -> String -> [Integer]
getIntgs 0 _ = []
getIntgs _ [] = []
getIntgs x str = map (\ s -> read s :: Integer) $ take x $ words str

getInts :: Int -> String -> [Int]
getInts 0 _ = []
getInts _ [] = []
getInts x str = map (\ s -> read s :: Int) $ take x $ words str

digs :: Integral a => a -> [a]
digs 0 = []
digs x = digs (x `div` 10) ++ [x `mod` 10]

main = do
    line <- getLine
    let (k:_) = getInts 1 line
    loop k 0

loop k n = do
    eof <- isEOF
    if eof then
        putStrLn $ foldl1 (++) $ map show $ take k $ digs n
    else do
        line <- getLine
        let (x:_) = getIntgs 1 line
        loop k $ n + x
