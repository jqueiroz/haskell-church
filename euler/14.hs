collatz :: (Integral a) => a -> [a] -> [a]
collatz 0 ls = ls
collatz 1 ls = ls
collatz n ls
    | even n = collatz (div n 2) (n:ls)
	| otherwise = collatz (3*n + 1) (n:ls)

solve :: (Integral a) => a -> a
solve input = n
    where (n, _) = foldl (\ (acum, lcol) x -> let lcolx = length (collatz x []) in if lcolx > lcol then (x, lcolx) else (acum, lcol) ) (0,0) [1..input]

main = do
    line <- getLine
    let k = read line :: Integer
    print $ solve k
