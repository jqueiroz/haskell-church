digs :: Integral a => a -> [a]
digs 0 = []
digs x = x `mod` 10 : digs (x `div` 10)

getIntgs :: String -> [Integer]
getIntgs [] = []
getIntgs str = map (\ s -> read s :: Integer) $ words str

solve :: Integral a => a -> a -> a
solve x y = sum $ digs $ x^y

main = do
    line <- getLine
    let (x:y:_) = take 2 $ getIntgs line
    print $ solve x y
