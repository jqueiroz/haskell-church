solve input = foldl (\ acum k -> if even k then acum + k else acum ) 0 $ reverse $ fibonacci input [2,1]

fibonacci :: (Integral a) => a -> [a] -> [a]

fibonacci n fs@(top:pretop:rs)
    | top + pretop > n = fs
    | otherwise = fibonacci n ((top + pretop) : fs)
