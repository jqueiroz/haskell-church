solve input = last [p | p <- factors input, isPrime p]

factors :: (Integral a) => a -> [a]

factors n = [k | k <- [1..n], mod n k == 0]

isPrime :: (Integral a) => a -> Bool

isPrime 1 = False
isPrime 2 = True
isPrime n
    | any (\x -> mod n x == 0) ([3,5..sqrt_n]) = False
    | otherwise = True
    where sqrt_n = truncate $ sqrt $ fromIntegral n
