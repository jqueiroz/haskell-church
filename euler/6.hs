solve input = (sumRange 1 input)^2 - sum [k^2 | k <- [1..input]]

sumRange :: (Integral a) => a -> a -> a

sumRange l r = ((l + r)*(l + r - 1)) `div` 2

-- sumSquaredRange :: (Integral a) => a -> a -> a
--
-- sumSquaredRange l r =
