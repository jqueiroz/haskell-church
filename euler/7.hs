solve input = [k | k <- [1..], isPrime k] !! (input - 1)

isPrime :: (Integral a) => a -> Bool

isPrime n
    | any (\x -> mod n x == 0) (2:[3,5..sqrt_n]) = False
    | otherwise = True
    where sqrt_n = truncate $ sqrt $ fromIntegral n
