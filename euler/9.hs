solve :: (Integral a) => a -> a

solve input = ans input input input

ans :: (Integral a) => a -> a -> a -> a

ans _ 0 0 = -1
ans k m 0 = ans k (m - 1) (m - 1)
ans k m n
    | m*(m + n) == div k 2 = (m^2 - n^2)*(2*m*n)*(m^2 + n^2)
    | otherwise = ans k m (n - 1)
