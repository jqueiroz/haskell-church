import GHC.Int
import Control.Applicative ((<$>), (<*>))
import Control.Monad (guard)

-- extGCD a b = (x, y, g) such that g = ax + by (and g is the gcd)
minv a m = if x >= 0 then x else m+x where (x, y, _) = extGCD a m
extGCD :: Int64 -> Int64 -> (Int64, Int64, Int64)
extGCD 0 0 = error "extGCD(0,0) is undefined"
extGCD a 0 = (1,0,a)
extGCD a b = let (q,r) = (a `div` b, a `mod` b)
                 (c,x,y) = extGCD b r
         in  (x,c-q*x, y)

modulus :: Int64
modulus = 1000000000 + 7

newtype ModularInteger = ModularInteger { getModularInteger :: Int64 }
fromInt64 :: Int64 -> ModularInteger
fromInt64 x = ModularInteger . abs $ x `mod` modulus
inverse :: ModularInteger -> ModularInteger
inverse (ModularInteger x) = ModularInteger $ minv x modulus
(//) :: ModularInteger -> ModularInteger -> ModularInteger
a // b = a * (inverse b)
instance Num ModularInteger where
    (ModularInteger a) + (ModularInteger b) = ModularInteger $ (a + b) `mod` modulus
    (ModularInteger a) - (ModularInteger b) = ModularInteger $ (modulus + a - b) `mod` modulus
    (ModularInteger a) * (ModularInteger b) = ModularInteger $ (a * b) `mod` modulus
    fromInteger = fromInt64 . fromInteger
    abs x = x
    negate (ModularInteger x) = ModularInteger $ modulus - x
    signum x = ModularInteger 1
instance Show ModularInteger where
    show = show . getModularInteger

-- TODO: define a +? b = Sum a b, a *? b = Product a b, etc. (along with precedence)
-- Polynomial [(a1, k1), ...] = a1 * z^k1 + ...
-- GeneralizedAccumulator k   = 1 / (1-z)^k
-- TODO: remove EmptyAccumulator and just use GeneralizedAccumulator 0 instead; maybe i'll need to change getGene...Term
newtype Polynomial = Polynomial [(ModularInteger, Int64)]
newtype GeneralizedAccumulator = GeneralizedAccumulator Int64
multiplyPolynomial :: Polynomial -> Polynomial -> Polynomial
multiplyGeneralizedAccumulator :: GeneralizedAccumulator -> GeneralizedAccumulator -> GeneralizedAccumulator
(Polynomial xs) `multiplyPolynomial` (Polynomial ys) = Polynomial $ do
    (x, px) <- xs
    (y, py) <- ys
    return $ (x*y, px+py)
(GeneralizedAccumulator k1) `multiplyGeneralizedAccumulator` (GeneralizedAccumulator k2) = GeneralizedAccumulator $ k1 + k2

newtype GeneratingFunction = GeneratingFunction (Polynomial, GeneralizedAccumulator)
identity = fromPolynomial $ Polynomial [(1, 0)]
accumulator = fromGeneralizedAccumulator $ GeneralizedAccumulator 1

multiply :: GeneratingFunction -> GeneratingFunction -> GeneratingFunction
fromPolynomial :: Polynomial -> GeneratingFunction
fromGeneralizedAccumulator :: GeneralizedAccumulator -> GeneratingFunction
(GeneratingFunction (p1, a1)) `multiply` (GeneratingFunction (p2, a2)) = GeneratingFunction (p3, a3) where
    p3 = p1 `multiplyPolynomial` p2
    a3 = a1 `multiplyGeneralizedAccumulator` a2
fromPolynomial p = GeneratingFunction (p, GeneralizedAccumulator 0)
fromGeneralizedAccumulator a = GeneratingFunction ((Polynomial [(1, 0)]), a)

getTerm :: Int64 -> GeneratingFunction -> ModularInteger
getTerm n (GeneratingFunction (Polynomial xs, GeneralizedAccumulator 0)) = sum . map fst . filter ((== n) . snd) $ xs
getTerm n (GeneratingFunction (Polynomial xs, GeneralizedAccumulator k)) = sum . map getRealTerm $ xs where
    getRealTerm (x, px) = x * (getGeneralizedAccumulatorTerm $ n-px)
    getGeneralizedAccumulatorTerm p = if p < 0 then 0 else num // den where
        num = product . map fromInt64 $ [(p+1)..(p+k-1)]
        den = product . map fromInt64 $ [2..(k-1)]

box :: Int64 -> GeneratingFunction
box f = lhs `multiply` rhs where
    lhs = fromPolynomial $ Polynomial [(1, 0), (-1, f+1)]
    rhs = accumulator

solve :: Int64 -> [Int64] -> ModularInteger
solve s fs = getTerm s $ foldr multiply identity $ map box fs

main = do
    (_:s:[]) <- (map read) . words <$> getLine
    fs <- (map read) . words <$> getLine
    putStrLn . show $ solve s fs
