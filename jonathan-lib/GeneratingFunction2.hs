import GHC.Int
import Control.Applicative ((<$>), (<*>))
import Control.Monad (guard)

-- extGCD a b = (x, y, g) such that g = ax + by (and g is the gcd)
minv a m = if x >= 0 then x else m+x where (x, y, _) = extGCD a m
extGCD :: Int64 -> Int64 -> (Int64, Int64, Int64)
extGCD 0 0 = error "extGCD(0,0) is undefined"
extGCD a 0 = (1,0,a)
extGCD a b = let (q,r) = (a `div` b, a `mod` b)
                 (c,x,y) = extGCD b r
         in  (x,c-q*x, y)

modulus :: Int64
modulus = 1000000000 + 7

newtype ModularInteger = ModularInteger { getModularInteger :: Int64 }
fromInt64 :: Int64 -> ModularInteger
fromInt64 x = ModularInteger . abs $ x `mod` modulus
inverse :: ModularInteger -> ModularInteger
inverse (ModularInteger x) = ModularInteger $ minv x modulus
(//) :: ModularInteger -> ModularInteger -> ModularInteger
a // b = a * (inverse b)
instance Num ModularInteger where
    (ModularInteger a) + (ModularInteger b) = ModularInteger $ (a + b) `mod` modulus
    (ModularInteger a) - (ModularInteger b) = ModularInteger $ (modulus + a - b) `mod` modulus
    (ModularInteger a) * (ModularInteger b) = ModularInteger $ (a * b) `mod` modulus
    fromInteger = fromInt64 . fromInteger
    abs x = x
    negate (ModularInteger x) = ModularInteger $ modulus - x
    signum x = ModularInteger 1
instance Show ModularInteger where
    show = show . getModularInteger

-- TODO: define a +? b = Sum a b, a *? b = Product a b, etc. (along with precedence)
data GeneratingFunction = Polynomial ModularInteger Int64 | GeneralizedAccumulator Int64 | Sum GeneratingFunction GeneratingFunction | Subtraction GeneratingFunction GeneratingFunction | Product GeneratingFunction GeneratingFunction
identity = Monomial 1 0
accumulator = GeneralizedAccumulator 1
-- Monomial a k        = a * z^k
-- Accumulator k       = 1 / (1-z)^k
get_term :: Int64 -> GeneratingFunction -> ModularInteger
get_term n _ | n < 0 = 0
get_term n (Monomial a k) = if k == n then a else 0
get_term _ (GeneralizedAccumulator 0) = error "No generalized accumulator for k=0"
get_term n (GeneralizedAccumulator k) = num // den where
    num = product . map fromInt64 $ [(n+1)..(n+k-1)]
    den = product . map fromInt64 $ [2..(k-1)]
get_term n (Sum u v) = (get_term n u) + (get_term n v)
get_term n (Subtraction u v) = (get_term n u) - (get_term n v)
get_term n (Product (Monomial a k) u) = a * (get_term (n-k) u)
get_term n (Product u (Monomial a k)) = get_term n $ Product (Monomial a k) u
get_term n (Product (GeneralizedAccumulator k1) (GeneralizedAccumulator k2)) = get_term n $ GeneralizedAccumulator (k1*k2)
get_term n (Product u v) = sum $ map f [0..n] where
    f i = (get_term i u) * (get_term (n-i) v)

box :: Int64 -> GeneratingFunction
box f = Product lhs rhs where
    lhs = Subtraction identity (Monomial 1 $ f + 1)
    rhs = accumulator

solve :: Int64 -> [Int64] -> ModularInteger
solve s fs = get_term s $ foldr Product identity $ map box fs

main = do
    (_:s:[]) <- (map read) . words <$> getLine
    fs <- (map read) . words <$> getLine
    putStrLn . show $ solve s fs
