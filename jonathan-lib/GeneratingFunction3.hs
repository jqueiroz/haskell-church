import GHC.Int
import Control.Applicative ((<$>), (<*>))
import Control.Monad (guard)

-- extGCD a b = (x, y, g) such that g = ax + by (and g is the gcd)
minv a m = if x >= 0 then x else m+x where (x, y, _) = extGCD a m
extGCD :: Int64 -> Int64 -> (Int64, Int64, Int64)
extGCD 0 0 = error "extGCD(0,0) is undefined"
extGCD a 0 = (1,0,a)
extGCD a b = let (q,r) = (a `div` b, a `mod` b)
                 (c,x,y) = extGCD b r
         in  (x,c-q*x, y)

modulus :: Int64
modulus = 1000000000 + 7

newtype ModularInteger = ModularInteger { getModularInteger :: Int64 }
fromInt64 :: Int64 -> ModularInteger
fromInt64 x = ModularInteger . abs $ x `mod` modulus
inverse :: ModularInteger -> ModularInteger
inverse (ModularInteger x) = ModularInteger $ minv x modulus
(/) :: ModularInteger -> ModularInteger -> ModularInteger
a / b = a * (inverse b)
instance Num ModularInteger where
    (ModularInteger a) + (ModularInteger b) = ModularInteger $ (a + b) `mod` modulus
    (ModularInteger a) - (ModularInteger b) = ModularInteger $ (modulus + a - b) `mod` modulus
    (ModularInteger a) * (ModularInteger b) = ModularInteger $ (a * b) `mod` modulus
    fromInteger = fromInt64 . fromInteger
    abs x = x
    negate (ModularInteger x) = ModularInteger $ modulus - x
    signum x = ModularInteger 1
instance Show ModularInteger where
    show = show . getModularInteger

data GeneratingFunction = Monomial ModularInteger Int64 | IdentityPower Int64 | Sum GeneratingFunction GeneratingFunction | Subtraction GeneratingFunction GeneratingFunction | Product GeneratingFunction GeneratingFunction
identity = IdentityPower 1
-- Monomial a k        = a * z^k
-- IdentityPower k     = 1 / (1-z)^k
------ Polynomial [(a, k)] = a_1*z^k1 + a_2*z^k2 + ...
get_term :: Int64 -> GeneratingFunction -> ModularInteger
get_term n _ | n < 0 = 0
get_term n (Monomial a k) = if k == n then a else 0
get_term _ (IdentityPower 0) = error "No identity power for zero"
get_term n (IdentityPower k) = num * (inverse den) where
    num = product . map fromInt64 $ [(n+1)..(n+k-1)]
    den = product . map fromInt64 $ [2..(k-1)]
get_term n (Sum u v) = (get_term n u) + (get_term n v)
get_term n (Subtraction u v) = (get_term n u) - (get_term n v)
get_term n (Product u v) = (get_term n u) * (get_term n v)
{-get_term n (Polynomial vs) = sum $ map (get_term n . uncurry Monomial) $ vs-}
{-get_term n (Product (Monomial a k) u) = a * (get_term (n-k) u)-}
{-get_term n (Product u (Monomial a k)) = get_term n $ Product (Monomial a k) u-}
{-get_term n (Product (Polynomial vs) u) = sum $ map (get_term n . Product u . uncurry Monomial) vs-}
{-get_term n (Product u (Polynomial vs)) = get_term n $ Product (Polynomial vs) u-}
{-get_term n (Product (Monomial a1 k1) (Monomial a2 k2)) = get_term n $ Monomial (a1*a2) (k1+k2)-}
{-get_term n (Product (Monomial a1 k1) (Polynomial vs)) = get_term n $ Polynomial $ map (\(a2, k2) -> (a1*a2, k1+k2)) vs-}
{-get_term (Polynomial vs) n = sum . map fst . filter ((==n) . snd) $ vs-}
{-get_term (Product (Monomial a k) u) = get_term $ Product (Polynomial [(a, k)]) u-}
{-get_term (Product u (Monomial a k)) = get_term $ Product u (Polynomial [(a, k)])-}
{-get_term (Product (Polynomial us) (Polynomial vs)) n = sum $ do-}
    {-(ua, uk) <- us-}
    {-(va, vk) <- vs-}
    {-guard $ uk + vk == n-}
    {-return $ ua*va-}

box :: Int64 -> GeneratingFunction
box f = Product lhs rhs where
    lhs = Subtraction (Monomial 1 0) (Monomial 1 f)
    rhs = identity

solve :: Int64 -> [Int64] -> ModularInteger
solve s fs = get_term s $ foldr (Product . box) identity fs

test = solve 3 [3, 4]

main = do
    (n:f:[]) <- ((map read) . words) <$> getLine
    putStrLn . show $ (n :: Integer)
    putStrLn "oi"
{-main = putStrLn . show . fat . fromInteger $ modulus `div` 2-}
