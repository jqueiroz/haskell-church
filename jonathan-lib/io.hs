import qualified Data.ByteString.Char8 as BS

main = BS.interact $ BS.pack . show . sum . map readInt . BS.words

readInt :: BS.ByteString -> Int
readInt x =
    case BS.readInt x of Just (i,_) -> i
                         Nothing    -> error "Unparsable Int"
