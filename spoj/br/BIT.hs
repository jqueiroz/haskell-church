import Control.Monad
import Data.List
import Control.Applicative

main = do
    ls <- init . map (read :: String -> Int) . lines <$> getContents
    mapM_ (\(a, ndt) -> putStrLn $ "Teste " ++ show ndt ++ "\n" ++ (unwords . map show $ a) ++ "\n") $ tail $ scanl (\(_, n) xs -> (xs, n + 1)) ([], 0) $ decompose [50, 10, 5, 1] <$> ls

decompose :: [Int] -> Int -> [Int]
decompose [] _ = []
decompose bs n = q : decompose (tail bs) r
    where
        (q, r) = quotRem n (head bs)
