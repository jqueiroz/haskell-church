import Control.Monad

main = solve 1

solve ndt = do
    line <- getLine
    let n = read line :: Int
    if n == 0 then do
        return ()
    else do
        allines <- sequence $ replicate n getLine
        putStr "Teste "
        print ndt
        mapM_ print $ scanl1 (+) $ map ((\ (a, b) -> (read a) - (read b) ) . (\ (a:b:[]) -> (a, b)) . words) allines
        putStrLn ""
        solve $ ndt + 1
