import Control.Monad
import Control.Applicative

main = getLine >>= print . (\x -> x + 76 - mod (x - 1986) 76) . (read :: String -> Int)
