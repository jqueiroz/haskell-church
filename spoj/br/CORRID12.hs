import Control.Monad

main = do
    line <- getLine
    let nums = map (\a -> read a :: Int) $ words line
        c = head nums
        n = last nums
    print $ c `mod` n
