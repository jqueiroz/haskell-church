import Control.Monad
import Control.Applicative

main = getLine >>= return . map (read :: String -> Int) . words >>= (\[x1, y1, x2, y2] -> print $ abs (x1 - x2) + abs (y1 - y2))
