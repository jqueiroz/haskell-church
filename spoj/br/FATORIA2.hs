import Control.Applicative
import Control.Monad

main = getLine >>= print . product . (\x -> [1..x]) . (read :: String -> Int)
