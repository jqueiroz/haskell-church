import Control.Monad

main = do
    line <- getLine
    let x = head $ map (\s -> read s :: Int) $ words line
    print $ 4*x
