import Control.Monad

main = do
    line <- getLine
    putStrLn $ solve line

solve :: String -> String
solve ('0':ss) = "C"
solve "1 0" = "B"
solve _ = "A"
