import Control.Monad
import Data.List
import Control.Applicative
import System.IO

main = getContents >>= return . map words . tail . lines >>= return . map (map (read :: String -> Int)) >>= return . fmap (\[l, c] -> guard (l > c) >> [c]) >>= print . sum . concat
