import Control.Monad

main = do
    slist <- sequence [getLine, getLine]
    let nlist = map (\s -> read s :: Int) slist 
    print $ (nlist !! 0) - (nlist !! 1)
