import Control.Monad
import Control.Applicative

main = do
    [l, d] <- getLine >>= return . map (read :: String -> Int) . words
    [k, p] <- map (read :: String -> Int) . words <$> getLine
    print $ l*k + p*(div l d)
