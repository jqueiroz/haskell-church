import Control.Monad

main = getLine >>= putStrLn . isPrime . (read :: String -> Int)

isPrime :: (Integral a) => a -> String
isPrime 2 = "sim"
isPrime 1 = "nao"
isPrime n
	| any (\m -> mod n m == 0) (2:[3,5..n - 1]) = "nao"
    | otherwise = "sim"
