import Control.Monad
import Control.Applicative

main = getLine >>= print . (^2) . ((read :: String -> Int) <$> words)
