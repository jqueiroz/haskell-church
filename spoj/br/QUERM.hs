import Control.Monad

main = solve 1

solve ndt = do
    line <- getLine
    let n = read line :: Int
    if n == 0 then do
        return ()
    else do
        line <- getLine
        let xs = map (\s -> read s :: Int) $ words line
            (_, ans) = foldl (\ (idx, acum) x -> (idx + 1, if x == idx then x else acum) ) (1, 0) xs
        putStr "Teste "
        print ndt
        print ans
        putStrLn ""
        solve $ ndt + 1
