import Control.Monad

main = do
    line <- getLine
    let n = read line :: Int
    nums <- sequence $ take n $ repeat getLine
    print $ foldr1 (+) $ map (\s -> read s :: Int) nums
