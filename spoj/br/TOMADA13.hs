import Control.Monad
import Control.Applicative

main = getLine >>= return . map (read :: String -> Int) . words >>= print . (\x -> x - 3) . sum 
