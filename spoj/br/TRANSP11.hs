import Control.Monad
import Control.Applicative

main = do
    n <- map (read :: String -> Int) . words <$> getLine
    m <- map (read :: String -> Int) . words <$> getLine
    print $ product $ getZipList $ div <$> ZipList m <*> ZipList n
