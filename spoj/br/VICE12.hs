import Control.Monad
import Data.List
import Control.Applicative

main = getLine >>= print . (flip (!!) 1) . sort . map (read :: String -> Int) . words
